import Ember from 'ember';

export default Ember.Route.extend({
  model() {
  //return this.store.find('category', 1);
   return this.get('store').findAll('category');
 },
 actions: {
   delete(category) {
     category.deleteRecord();
     category.save();
   }
 }
});
