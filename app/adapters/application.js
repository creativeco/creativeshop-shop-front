import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  host: 'http://shop.dev/api/v1'
  //host: 'https://printershop.herokuapp.com/api/v1'
});
