import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {

  this.route('categories', { path: '/' }, function(){
    this.route('new', { path: 'new' });
    this.route('edit', { path: '*id/edit' });
    this.route('show', { path: '*id' });
  });
  this.route('fourohfour', { path: '404' } );
});

export default Router;
