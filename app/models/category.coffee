`import DS from 'ember-data'`
category = DS.Model.extend
  name: DS.attr 'string'
  description: DS.attr 'string'
  main: DS.attr 'boolean'
  ancestors: DS.hasMany 'category', {inverse: 'kids', async: true}
  kids: DS.hasMany 'category', {inverse: 'parent', async: true}
  parent: DS.belongsTo 'category', {inverse: 'kids', async: true}
`export default category`



# `import DS from 'ember-data'`
#
# category = DS.Model.extend
#   name: DS.attr 'string'
#   description: DS.attr 'string'
#   parents: DS.hasMany 'group'
#   subgroups: DS.hasMany 'group'
#   main: DS.attr 'boolean'
#
# `export default category`


# import DS from 'ember-data';
# export default DS.Model.extend({
#   name: DS.attr('string'),
#   description: DS.attr 'string'
#   main: DS.attr('boolean'),
#   parents: DS.hasMany('group'),
#   subgroups: DS.hasMany('group'),
#   ancestors:      DS.hasMany('category', {
#     inverse: 'kids',
#     async: true
#   }),
#   kids:           DS.hasMany('category', {
#     inverse: 'parent',
#     async: true
#   }),
#   parent:         DS.belongsTo('category', {
#     inverse: 'kids',
#     async: true
#   })
# });
