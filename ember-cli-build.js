/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var Funnel = require('broccoli-funnel');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
  });
  var materializeFonts = new Funnel('public/assets/', {
  srcDir: '/',
  include: ['**/*.*'],
  destDir: '/assets'
  });

  return app.toTree([materializeFonts]);
};
